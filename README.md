**Regrouper tous les fichiers .scss avec un "import" dans le fichier main.scss**

# To-do création du site web
## Aide :
- Utiliser commentaire dans les fichiers Javascript = JSDoc
- (Personnel) Ouvrir dossier "Outils"
- [Grid System](https://github.com/TheveninKillian/grid-css)
- [Can I Use](https://caniuse.com/)
- [MDN](https://developer.mozilla.org/fr/)
- Si possible ajouter directement le CSS dans **les balises HTML**
- [Hack IE](http://www.blog.stephanejacquot.com/css/article/hacks-css-pour-ie-10-et-11)
- Polyfill
- [Autoprefixer](https://autoprefixer.github.io)
- **Ajouter aliases Git dans le fichier .git/config**
```
a = add
aa = add .
st = status
cm = commit -m
cam = commit -a -m
pu = push -u
puo = push -u origin
puom = push -u origin master
cl = clone
l = log
config-l-l = config --local -l
config-l-e = config --local -e
```
- [Serveur **Netlify**](https://www.netlify.com/docs/welcome/)

---
## Commencer le projet :
- **Lire le cahier des charges**
- **Définir son projet :**
  - **Choix des APIs, Librairies, Frameworks, Tools...**
  - **Choix des navigateurs supportés**
  - (**Charte graphique** couleur, polices...)
- **Création d'un prototype :**
(Personnel) Trello > Voir fiche CSS Orienté Object
  - Commencer à penser à la **sémantique** HTML et CSS
  - Penser a l'**héritage** CSS
  - Penser à **factoriser** le CSS
- **La taille du projet :**
  - **Petit projet :** [Initializr](http://www.initializr.com/)
  - **Grand projet :** [Yeoman](http://yeoman.io/), **Boilerplate**
- **Type de projet :**
  - **Experimentaux :** Aucune limite
  - **Production :** Performance, Réferencement, Accessibilté, Sécurité
- **Structure des dossiers du projets**
- **Créer les class, object, mixins, variables... CSS**
- **Utilisation : CSS Mega System**
  - [SEM et BIO](https://css-tricks.com/combining-the-powers-of-sem-and-bio-for-improving-css/?utm_source=CSS-Weekly&utm_campaign=Issue-318&utm_medium=web)
  - [CSS Keys](https://pyx.space/post/keys-to-maintainable-css-order)
- [System Git](https://buzut.fr/git-bien-nommer-ses-commits/)
- **Utilisation du mobile-first/Generic-first**

## Après avoir fini le projet :
- **Checklist**
  - http://webdevchecklist.com/
  - https://frontendchecklist.io/
  - https://github.com/thedaviddias/Front-End-Performance-Checklist
  - https://www.matthieu-tranvan.fr/search-engine-optimization-seo/checklist-seo-25-bonnes-pratiques-on-site-pour-renforcer-votre-referencement-naturel.html
- [**Réferencement Naturel**](http://www.eskimoz.fr/referencement-naturel/)
  - ~300 mots par pages
  - Bien choisir ces mots clés
- [**Test SEO**](https://varvy.com/)
- **Accessibilité**
  - [lien 1](https://medium.com/@realabhijeet4u/9-tips-to-get-bare-minimum-of-web-accessibility-739899a9437c)
  - [lien 2](http://a11y-style-guide.com/style-guide/)
  - [lien 3](http://accessibility.voxmedia.com/#engineers)
- **Performance**
  - DevTools (network/audits)
  - (Personnel) Trello > Voir fiche Performance

---
## Tools
[Front-end Tools](http://frontendtools.com),
[Devhints.io](https://devhints.io),
[DevDocs](http://devdocs.io/)